<?php
    session_start();
    include("conexion.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Landing Page - Start Bootstrap Theme</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">            
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="#">SoftRest</a>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">                    
                    <li>
                        <a href="#">Administrar</a>
                    </li>              
                    <li>
                        <a id='resultado'>
                            <?php                        
                                if(!empty($_SESSION['usuario'])){    
                                    echo "<li>";
                                    echo $_SESSION['usuario'];
                                    echo "</li>"; 
                                    echo "<li><a href='logout.php'>Salir</a></li>";                                   
                                }
                            ?>  
                        </a>
                    </li>          
                </ul>
            </div>
           
            <navbar-collapse>
        </div>
        <container>
    </nav>
    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>SoftRest</h1>
                        <h3>Ocupate de tus clientes, nosotros nos preocupamos por tu negocio.</h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                            <li>
                                <a href="#services" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Reservar Mesa</span></a>
                            </li>
                            <li>
                                <a href="#ordenar" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Ordenar</span></a>
                            </li>
                            <li>
                                <a href="#pagar" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Pagar</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>

    <!-- /.intro-header -->

    <!-- Page Content -->

	<a  name="services"></a>
    <div class="content-section-a">
        <div class="container">
            <div class="row" id="reservar">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Reservar una mesa</h2>
                    <form id="formulario-reserva">
                        <?php  
                            include 'conexion.php';
                            $query="SELECT * FROM reserva";
                            $consulta = mysqli_query($link,$query);
                            $j=1;
                            while($fila = mysqli_fetch_array($consulta)) {
                                $arrayMesas[$j]=$fila["estado"];
                                $j++;
                            }
                            mysqli_free_result($consulta);
                            mysqli_close($link);                  
                        ?>                        
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa1">
                                <span class="icon-unlocked "><p>1</p></span>
                                <input type="hidden" name="mesa1-name" value="<?=$arrayMesas[1]?>" id="mesa1-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa2">
                                <span class="icon-unlocked"><p>2</p></span>   
                                <input type="hidden" name="mesa2-name" value="<?=$arrayMesas[2]?>" id="mesa2-input">             
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa3">
                                <span class="icon-unlocked "><p>3</p></span>
                                <input type="hidden" name="mesa3-name" value="<?=$arrayMesas[3]?>" id="mesa3-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa4">
                                <span class="icon-unlocked"><p>4</p></span>             
                                <input type="hidden" name="mesa4-name" value="<?=$arrayMesas[4]?>" id="mesa4-input">   
                            </div>
                        </div>
                        <br><br><br><br>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa5">
                                <span class="icon-unlocked "><p>5</p></span>
                                <input type="hidden" name="mesa5-name" value="<?=$arrayMesas[5]?>" id="mesa5-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa6">
                                <span class="icon-unlocked"><p>6</p></span>                
                                <input type="hidden" name="mesa6-name" value="<?=$arrayMesas[6]?>" id="mesa6-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa7">
                                <span class="icon-unlocked "><p>7</p></span>
                                <input type="hidden" name="mesa7-name" value="<?=$arrayMesas[7]?>" id="mesa7-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa8">
                                <span class="icon-unlocked"><p>8</p></span>       
                                <input type="hidden" name="mesa8-name" value="<?=$arrayMesas[8]?>" id="mesa8-input">
                            </div>
                        </div>
                        <br><br><br><br>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa9">
                                <span class="icon-unlocked "><p>9</p></span>
                                <input type="hidden" name="mesa9-name" value="<?=$arrayMesas[9]?>" id="mesa9-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa10">
                                <span class="icon-unlocked"><p>10</p></span>
                                <input type="hidden" name="mesa10-name" value="<?=$arrayMesas[10]?>" id="mesa10-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa11">
                                <span class="icon-unlocked "><p>11</p></span>
                                <input type="hidden" name="mesa11-name" value="<?=$arrayMesas[11]?>" id="mesa11-input">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg-primary text-center mesa" id="mesa12">
                                <span class="icon-unlocked"><p>12</p></span>    
                                <input type="hidden" name="mesa12-name" value="<?=$arrayMesas[12]?>" id="mesa12-input"> 
                            </div>
                        </div>
                        <input type="submit" name="submit" id="btn-reservar" value="Reservar">                        
                    </form>
                    <div id="resultado1"></div>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="img/food.jpg" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
    <a  name="ordenar"></a>    
    <div class="content-section-b">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Orden</h2>                     
                    <span id="orden"></span> 
                    <input type="hidden" class="nombre_item" value=""> 
                    <input type="hidden" class="precio_item" value=""> 
                    <button type="button" class="btn btn-default id_item"  value="">Agregar</button>     
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">  
                    <h2 class="section-heading">Menú</h2>                
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#platillos" data-toggle="tab" aria-expanded="false">PLATILLOS</a></li>
                      <li class=""><a href="#bebidas" data-toggle="tab" aria-expanded="true">BEBIDAS</a></li>
                      <li class=""><a href="#postres" data-toggle="tab" aria-expanded="true">POSTRES</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                      <div class="tab-pane fade active in" id="platillos">
                        <?php
                            include 'conexion.php';
                            $query="SELECT * FROM platillo"; 
                            $consulta = mysqli_query($link,$query);
                            ?>               
                        <table class="table table-striped table-hover ">
                          <thead>
                            <tr>                    
                              <th>Clave</th>
                              <th>Nombre</th>
                              <th>Precio</th>
                              <th>Ingredientes</th>
                            </tr>
                          </thead>
                          <tbody>                            
                            <?php                                
                                while ($fila = mysqli_fetch_array($consulta)){
                                    $id_platillo=$fila['id_platillo'];
                                    $nombre=$fila['nombre'];
                                    $precio=$fila['precio'];
                                    $ingredientes=$fila['ingredientes'];?>
                                    <tr class="warning">
                                        <td>
                                            <a href="#" data-idregistro="<?= $id_platillo?>" data-nombre="<?= $nombre?>" data-precio="<?= $precio?>" class="item-menu-platillo"><?= $id_platillo?></a>
                                        </td>
                                        <td>
                                            <span><?= $nombre?></span>
                                        </td>
                                        <td>
                                            <span><?= $precio?></span>
                                        </td>
                                        <td>
                                            <span> <?= $ingredientes?></span>
                                        </td>
                                    </tr>    
                            <?php                                       
                                }  
                                mysqli_free_result($consulta);
                                mysqli_close($link);
                            ?>                                            
                          </tbody>
                        </table>                                              
                      </div>
                      <div class="tab-pane fade" id="bebidas">
                        <?php
                            include 'conexion.php';
                            $query="SELECT * FROM bebida"; 
                            $consulta = mysqli_query($link,$query);
                            ?>               
                        <table class="table table-striped table-hover ">
                          <thead>
                            <tr>                    
                              <th>Clave</th>
                              <th>Nombre</th>
                              <th>Precio</th>
                              <th>Ingredientes</th>
                            </tr>
                          </thead>
                          <tbody>                            
                            <?php
                                while ($fila = mysqli_fetch_array($consulta)){
                                    $id_bebida=$fila['id_bebida'];
                                    $nombre=$fila['nombre'];
                                    $precio=$fila['precio'];
                                    $ingredientes=$fila['ingredientes'];?>
                                    <tr class="warning">
                                      <td>
                                        <a href="#" data-idregistro="<?= $id_bebida?>" data-nombre="<?= $nombre?>" data-precio="<?= $precio?>" class="item-menu-bebida"><?= $id_bebida?></a>
                                        </td>
                                      <td><span><?= $nombre?></span></td>
                                      <td><span><?= $precio?></span></td>
                                      <td><span><?= $ingredientes?></span></td>             
                                    </tr>    
                            <?php                                      
                                }  
                                mysqli_free_result($consulta);
                                mysqli_close($link);
                            ?>                                            
                          </tbody>
                        </table>
                      </div>
                      <div class="tab-pane fade" id="postres">
                        <?php
                            include 'conexion.php';
                            $query="SELECT * FROM postre"; 
                            $consulta = mysqli_query($link,$query);
                            ?>               
                        <table class="table table-striped table-hover ">
                          <thead>
                            <tr>                    
                              <th>Clave</th>
                              <th>Nombre</th>
                              <th>Precio</th>
                              <th>Ingredientes</th>
                            </tr>
                          </thead>
                          <tbody>                            
                            <?php
                                while ($fila = mysqli_fetch_array($consulta)){
                                    $id_postre=$fila['id_postre'];
                                    $nombre=$fila['nombre'];
                                    $precio=$fila['precio'];
                                    $ingredientes=$fila['ingredientes'];?>
                                    <tr class="warning">
                                      <td>
                                          <a href="#" data-idregistro="<?= $id_postre?>" data-nombre="<?= $nombre?>" data-precio="<?= $precio?>" class="item-menu-postre"><?= $id_postre?></a>
                                      </td>
                                      <td><span><?= $nombre?></span></td>
                                      <td><span><?= $precio?></span></td>
                                      <td><span><?= $ingredientes?></span></td>             
                                    </tr>    
                            <?php                                      
                                }  
                                mysqli_free_result($consulta);
                                mysqli_close($link);
                            ?>                                            
                          </tbody>
                        </table>
                      </div>  
                    </div> 

                </div>
            </div>
        </div> 
    </div>
  
    <a  name="pedido"></a>
    <div class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Pedido</h2>
                    <p class="lead">Elementos a cargar en la comanda</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                   <span id='orden'>
                        <table class="table table-striped table-hover" id="js-tabla">
                          <thead>
                            <tr>                    
                              <th>Clave</th>
                              <th>Nombre</th>
                              <th>Precio</th>
                            </tr>
                          </thead>
                          <tbody>                            
                                                                                           
                          </tbody>
                        </table>
                    </span>
                </div>
            </div>
        </div>
    </div>
    
    <a  name="pagar"></a>
    <div class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Pagar</h2>
                    <p class="lead">Sección de pago.</p>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="img/dessert.jpg" alt="">
                </div>
            </div>
        </div>
    </div>


	<a  name="contact"></a>
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Connect to Start Bootstrap:</h2>
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                            <a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Reservar Mesa</span></a>
                        </li>
                        <li>
                            <a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
  
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                        <a href="#ventanalogin" data-toggle="modal" >Botón 1</a>
                        <div class="modal fade" id="ventanalogin">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <!--Header de la ventana-->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">
                                            Iniciar sesion                           
                                        </h4>
                                    </div>
                                    <!--contenido de la ventana-->
                                    <div class="modal-body">
                                        <form id="formulario-login">
                                            <label for="usuario"></label>
                                            <input type="text" name="usuario" id="usuario" placeholder="Usuario" required/>
                                            <br><br>
                                            <label for="contrasena"></label>
                                            <input type="password" name="contrasena" id="contrasena" placeholder="Contraseña" required/>
                                        
                                    </div>
                                    <!--footer de la ventana-->
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" id="btn-ingresar" data-dismiss="modal">Ingresar</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="" id="sds">About</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#services">Services</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#contact">Contact</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; Your Company 2014. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>
    <script>
    $(document).ready(function() {
        $('.id_item').click(function(){
            ///var cadena= $(this).val();
            var element = $('.id_item').val();
            var cadena = element.split(',');
            $('#js-tabla tr:last').after('<tr class="warning"><td>'+cadena[0]+'</td><td>'+cadena[1]+'</td><td>'+cadena[2]+'</td></tr><tr><td>total</td></tr>');

        });         
    });    
    </script>
    <script>
    $(document).ready(function() {
        /*--------------al dar clic en un elemento del menu (postre)-----------*/
        $('.item-menu-postre').click(function(e){
            e.preventDefault();            
            var dato1 = $(this).attr('data-idregistro');              
            var dato2 = $(this).attr('data-nombre');              
            var dato3 = $(this).attr('data-precio');             
            var parametros1 = {
                "valor" : dato1
            };
            $.ajax({
                data:  parametros1,
                url:   'elegir-postre.php',
                type:  'post',
                beforeSend: function () {
                        //$(".span-platillo").html("Procesando, espere por favor...");
                },
                success:  function (response){                      
                    $('.id_item').val(dato1+','+dato2+','+dato3);   
                    $('#orden').html(response+'<img src="img/postres/'+dato1+'.jpg'+'" class="img-responsive" />');
                }
            });
            return false;
        });        
    });    
    </script>
    <script>
    $(document).ready(function() {
        /*--------------al dar clic en un elemento del menu (bebida)-----------*/
        $('.item-menu-bebida').click(function(e){
            e.preventDefault();                        
            var dato1 = $(this).attr('data-idregistro');              
            var dato2 = $(this).attr('data-nombre');              
            var dato3 = $(this).attr('data-precio');             
            var parametros1 = {
                "valor" : dato1
            };
            $.ajax({
                data:  parametros1,
                url:   'elegir-bebida.php',
                type:  'post',
                beforeSend: function () {
                        //$(".span-platillo").html("Procesando, espere por favor...");
                },
                success:  function (response){                      
                    $('.id_item').val(dato1+','+dato2+','+dato3);   
                    $('#orden').html(response+'<img src="img/bebidas/'+dato1+'.jpg'+'" class="img-responsive" />');
                }
            });
            return false;
        });        
    });    
    </script>
    <script>
    $(document).ready(function() {
        /*--------------al dar clic en un elemento del menu (platillo)-----------*/
        $('.item-menu-platillo').click(function(e){
            e.preventDefault();            
            var dato1 = $(this).attr('data-idregistro');              
            var dato2 = $(this).attr('data-nombre');              
            var dato3 = $(this).attr('data-precio');                   
            var parametros1 = {
                "valor" : dato1,
            };
            $.ajax({
                data:  parametros1,
                url:   'elegir-platillo.php',
                type:  'post',
                beforeSend: function () {
                        //$(".span-platillo").html("Procesando, espere por favor...");
                },
                success:  function (response){
                    $('.id_item').val(dato1+','+dato2+','+dato3);                                 
                    $('#orden').html(response+'<img src="img/comidas/'+dato1+'.jpg'+'" class="img-responsive" />');
                }
            });
            return false;
        });        
    });    
    </script>
    <script>
    $(document).ready(function() {
        /*--------------al dar clic en reservar-----------*/ 
        $('#formulario-reserva').on('submit',function(e){
            e.preventDefault();
            var mesa1 = $("input#mesa1-input").val();
            var mesa2 = $("input#mesa2-input").val();
            var mesa3 = $("input#mesa3-input").val();
            var mesa4 = $("input#mesa4-input").val();
            var mesa5 = $("input#mesa5-input").val();
            var mesa6 = $("input#mesa6-input").val();
            var mesa7 = $("input#mesa7-input").val();
            var mesa8 = $("input#mesa8-input").val();
            var mesa9 = $("input#mesa9-input").val();
            var mesa10 = $("input#mesa10-input").val();
            var mesa11 = $("input#mesa11-input").val();
            var mesa12 = $("input#mesa12-input").val();            
            var parametros = {
                "valorCaja1" : mesa1,
                "valorCaja2" : mesa2,
                "valorCaja3" : mesa3,
                "valorCaja4" : mesa4,
                "valorCaja5" : mesa5,
                "valorCaja6" : mesa6,
                "valorCaja7" : mesa7,
                "valorCaja8" : mesa8,
                "valorCaja9" : mesa9,
                "valorCaja10" : mesa10,
                "valorCaja11" : mesa11,
                "valorCaja12" : mesa12
            };
            $.ajax({
                data:  parametros,
                url:   'ordenar-mesa.php',
                type:  'post',
                beforeSend: function () {
                        $("#resultado1").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado1").html(response);
                }
            });
            return false;
        });
        /*------------------------------------------------*/
    });    
    </script>
    <script>
     $(document).ready(function() {
        /*--------------al dar clic en login-----------*/ 
        $('#btn-ingresar').click(function(){
            var usuario = $("input#usuario").val();
            var contrasena = $("input#contrasena").val();
            var dataString = 'usuario='+usuario+'&contrasena='+contrasena; 

            $.ajax({
                type:"POST",
                url: "login.php",
                data: dataString,
                success:function(response) {
                    $('#resultado').html(response);                    
                }
            });
            return false;
        });
        /*------------------------------------------------*/

    });
    </script>
    <script>
    $(document).ready(function () {
        //si al inicio hay un 1 o  0 pintamos los divs
        if($("#mesa1 input").val()==1){
            $("#mesa1").css("background","red");
        }else if($("#mesa1 input").val()==0){
            $("#mesa1").css("background","blue");
        }
        if($("#mesa2 input").val()==1){
            $("#mesa2").css("background","red");
        }else if($("#mesa2 input").val()==0){
            $("#mesa2").css("background","blue");
        }
        if($("#mesa3 input").val()==1){
            $("#mesa3").css("background","red");
        }else if($("#mesa3 input").val()==0){
            $("#mesa3").css("background","blue");
        }
        if($("#mesa4 input").val()==1){
            $("#mesa4").css("background","red");
        }else if($("#mesa4 input").val()==0){
            $("#mesa4").css("background","blue");
        }
        if($("#mesa5 input").val()==1){
            $("#mesa5").css("background","red");
        }else if($("#mesa5 input").val()==0){
            $("#mesa5").css("background","blue");
        }
        if($("#mesa6 input").val()==1){
            $("#mesa6").css("background","red");
        }else if($("#mesa6 input").val()==0){
            $("#mesa6").css("background","blue");
        }if($("#mesa7 input").val()==1){
            $("#mesa7").css("background","red");
        }else if($("#mesa7 input").val()==0){
            $("#mesa7").css("background","blue");
        }
        if($("#mesa8 input").val()==1){
            $("#mesa8").css("background","red");
        }else if($("#mesa8 input").val()==0){
            $("#mesa8").css("background","blue");
        }
        if($("#mesa9 input").val()==1){
            $("#mesa9").css("background","red");
        }else if($("#mesa9 input").val()==0){
            $("#mesa9").css("background","blue");
        }
        if($("#mesa10 input").val()==1){
            $("#mesa10").css("background","red");
        }else if($("#mesa10 input").val()==0){
            $("#mesa10").css("background","blue");
        }
        if($("#mesa11 input").val()==1){
            $("#mesa11").css("background","red");
        }else if($("#mesa11 input").val()==0){
            $("#mesa11").css("background","blue");
        }
        if($("#mesa12 input").val()==1){
            $("#mesa12").css("background","red");
        }else if($("#mesa12 input").val()==0){
            $("#mesa12").css("background","blue");
        }

        //al dar clic, se verifica que clase tiene y se cambia
        //si tiene una clase locked agrega al input 1, sino agrega un 0
        $("#mesa1").click(function(){
            $("#mesa1 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa1 span").hasClass("icon-locked")){
                $("#mesa1-input").val("1");                
                $("#mesa1").css("background","red");                

            }else    
            if($("#mesa1 span").hasClass("icon-unlocked")){
                $("#mesa1-input").val("0");                
                $("#mesa1").css("background","blue");                
            }
        });
        $("#mesa2").click(function(){
            $("#mesa2 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa2 span").hasClass("icon-locked")){
                $("#mesa2-input").val("1");                
                $("#mesa2").css("background","red");                
            }else    
            if($("#mesa2 span").hasClass("icon-unlocked")){
                $("#mesa2-input").val("0");                
                $("#mesa2").css("background","blue");                
            }
        });
        $("#mesa3").click(function(){
            $("#mesa3 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa3 span").hasClass("icon-locked")){
                $("#mesa3-input").val("1");  
                $("#mesa3").css("background","red");                              
            }else    
            if($("#mesa3 span").hasClass("icon-unlocked")){
                $("#mesa3-input").val("0"); 
                $("#mesa3").css("background","blue");                               
            }
        });
        $("#mesa4").click(function(){
            $("#mesa4 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa4 span").hasClass("icon-locked")){
                $("#mesa4-input").val("1");       
                $("#mesa4").css("background","red");                         
            }else    
            if($("#mesa4 span").hasClass("icon-unlocked")){
                $("#mesa4-input").val("0");                
                $("#mesa4").css("background","blue");                
            }
        });
        $("#mesa5").click(function(){
            $("#mesa5 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa5 span").hasClass("icon-locked")){
                $("#mesa5-input").val("1"); 
                $("#mesa5").css("background","red");                               
            }else    
            if($("#mesa5 span").hasClass("icon-unlocked")){
                $("#mesa5-input").val("0"); 
                $("#mesa5").css("background","blue");                               
            }
        });
        $("#mesa6").click(function(){
            $("#mesa6 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa6 span").hasClass("icon-locked")){
                $("#mesa6-input").val("1"); 
                $("#mesa6").css("background","red");                               
            }else    
            if($("#mesa6 span").hasClass("icon-unlocked")){
                $("#mesa6-input").val("0"); 
                $("#mesa6").css("background","blue");                               
            }
        });
        $("#mesa7").click(function(){
            $("#mesa7 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa7 span").hasClass("icon-locked")){
                $("#mesa7-input").val("1");
                $("#mesa7").css("background","red");                                
            }else    
            if($("#mesa7 span").hasClass("icon-unlocked")){
                $("#mesa7-input").val("0"); 
                $("#mesa7").css("background","blue");                               
            }
        });
        $("#mesa8").click(function(){
            $("#mesa8 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa8 span").hasClass("icon-locked")){
                $("#mesa8-input").val("1");
                $("#mesa8").css("background","red");                                
            }else    
            if($("#mesa8 span").hasClass("icon-unlocked")){
                $("#mesa8-input").val("0"); 
                $("#mesa8").css("background","blue");                               
            }
        });
        $("#mesa9").click(function(){
            $("#mesa9 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa9 span").hasClass("icon-locked")){
                $("#mesa9-input").val("1");
                $("#mesa9").css("background","red");                                
            }else    
            if($("#mesa9 span").hasClass("icon-unlocked")){
                $("#mesa9-input").val("0"); 
                $("#mesa9").css("background","blue");                               
            }
        });
        $("#mesa10").click(function(){
            $("#mesa10 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa10 span").hasClass("icon-locked")){
                $("#mesa10-input").val("1"); 
                $("#mesa10").css("background","red");                               
            }else    
            if($("#mesa10 span").hasClass("icon-unlocked")){
                $("#mesa10-input").val("0");
                $("#mesa10").css("background","blue");                                
            }
        });
        $("#mesa11").click(function(){
            $("#mesa11 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa11 span").hasClass("icon-locked")){
                $("#mesa11-input").val("1"); 
                $("#mesa11").css("background","red");                               
            }else    
            if($("#mesa11 span").hasClass("icon-unlocked")){
                $("#mesa11-input").val("0"); 
                $("#mesa11").css("background","blue");                               
            }
        });
        $("#mesa12").click(function(){
            $("#mesa12 span").toggleClass("icon-unlocked icon-locked");
            if($("#mesa12 span").hasClass("icon-locked")){
                $("#mesa12-input").val("1"); 
                $("#mesa12").css("background","red");                               
            }else    
            if($("#mesa12 span").hasClass("icon-unlocked")){
                $("#mesa12-input").val("0"); 
                $("#mesa12").css("background","blue");                               
            }
        });

    });
    </script>

</body>

</html>
 